using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mobilinput : MonoBehaviour
{
    public static mobilinput Instance { set; get; }// singelton olu�turduk


    private const float deadzone = 100.0f;
    private bool top, swipeleft, swiperight;
    private Vector2 swipedelta, starttouch;

    public bool Top { get { return top; } }
    public Vector2 Swipedelta { get { return swipedelta; } }
    public bool Swipeleft { get { return swipeleft; } }
    public bool Swiperight { get { return swiperight; } }
   
    private void Awake()
    {
        Instance = this;
    }

    public void Update()
    {
        top = swipeleft = swiperight  = false;



        if (Input.touches.Length != 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                top = true;
                starttouch = Input.mousePosition;

            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                starttouch = swipedelta = Vector2.zero;
            }
        }



        swipedelta = Vector2.zero;
        if (starttouch != Vector2.zero)//burada dokundugumuz mesafeler aras�n� �l�t�k
        {
            // telefon i�in 
            if (Input.touches.Length != 0)
            {
                swipedelta = Input.touches[0].position - starttouch;// ba�lang�� de�erimizden dokundugumuz yerin kordinatlar�n� ��kart�yoruz [0] ilk dokundugun yer birinci parmak demek 
            }


        }

        if (swipedelta.magnitude > deadzone)// vekt�r elemanlar�n�n karelerini al�p toplay�p karek�k�n� al�yor
        {
            float x = swipedelta.x;
            float y = swipedelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    swipeleft = true;
                }
                else
                    swiperight = true;
            }
           
            starttouch = swipedelta = Vector2.zero;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cubespawn : MonoBehaviour
{

    public GameObject cubeobj;
    public GameObject panel;
    Vector3 vec;
    AudioSource [] audio;
    private void Start()
    {
        vec = new Vector3(0,.5f,0);
        audio = GetComponents<AudioSource>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("feed")) 
        {
            Instantiate(cubeobj, transform.position + Vector3.up, transform.rotation);
            transform.position += vec;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("obstacle") && gameObject.transform.childCount<=1)
        {
            audio[0].Play();
            Time.timeScale = 0;
            panel.SetActive(true);
        }
        if (other.CompareTag("base") )
        {
            audio[1].Play();
            Time.timeScale = 0;
            panel.SetActive(true);
        }
    }
   public void retun()
    {
        SceneManager.LoadScene("lwl1");
        Time.timeScale = 1;
    }
   
}

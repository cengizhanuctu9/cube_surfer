using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motor : MonoBehaviour
{
    private const float lanedistance = 3.0f;
    private float verticalvelecity;
    private float speed = 14;
    private int desiretlane = 1;
 
    private void Update()
    {
        if (mobilinput.Instance.Swipeleft)
        {
            movelane(false);
        }
        if (mobilinput.Instance.Swiperight)
        {
            movelane(true);
        }
        Vector3 targetposition = transform.position.z * Vector3.forward;
        if (desiretlane == 0)
        {
            targetposition += Vector3.left * lanedistance;
        }
        else if (desiretlane == 2)
        {
            targetposition += Vector3.right * lanedistance;
        }
        Vector3 movevector = Vector3.zero;// kayma
        movevector.x = (targetposition - transform.position).normalized.x * 20;//speed yap


        movevector.y = verticalvelecity;
        movevector.z = speed;
        moveplayer(movevector * Time.deltaTime);

        if (transform.position.y < 1)
        {
            int countch = transform.childCount;
            transform.position += new Vector3(0, countch, 0);
        }

    }
    private void movelane(bool goingright)
    {
        desiretlane += (goingright) ? 1 : -1;
        desiretlane = Mathf.Clamp(desiretlane, 0, 2);

    }
    public void moveplayer(Vector3 vec)
    {
        transform.position += vec;
    }
}

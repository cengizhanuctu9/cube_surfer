using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public GameObject player;
    Vector3 dis;
    private void Start()
    {
        dis = transform.position - player.transform.position;

    }
    private void LateUpdate()
    {
        transform.position = player.transform.position + dis;
    }
}
